[
    {
        "description": null,
        "fields": [
            {
                "attributes": {
                    "cloudRequired": "true"
                },
                "isRequired": false,
                "name": "ObjectType",
                "transform": {
                    "attributes": {
                        "value": "User"
                    },
                    "type": "static"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "givenName",
                "transform": {
                    "attributes": {
                        "name": "firstname"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "sn",
                "transform": {
                    "attributes": {
                        "name": "lastname"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {
                    "cloudMaxUniqueChecks": "50",
                    "cloudRequired": "true",
                    "template": "CN=$(firstname) $(lastname)$(uniqueCounter),$(organizationalunit)"
                },
                "isRequired": false,
                "name": "distinguishedName",
                "transform": {
                    "attributes": {
                        "name": "Create Unique Account ID"
                    },
                    "type": "rule"
                },
                "type": "string"
            },
            {
                "attributes": {
                    "cloudMaxSize": "20",
                    "cloudMaxUniqueChecks": "50",
                    "cloudRequired": "true",
                    "template": "$(samaccountname)$(uniqueCounter)"
                },
                "isRequired": false,
                "name": "sAMAccountName",
                "transform": {
                    "attributes": {
                        "name": "Create Unique LDAP Attribute"
                    },
                    "type": "rule"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "displayName",
                "transform": {
                    "attributes": {
                        "name": "displayName"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {
                    "cloudRequired": "true"
                },
                "isRequired": false,
                "name": "manager",
                "transform": {
                    "attributes": {
                        "name": "Get Manager LDAP DN"
                    },
                    "type": "rule"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "pwdLastSet",
                "transform": {
                    "attributes": {
                        "value": "true"
                    },
                    "type": "static"
                },
                "type": "boolean"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "password",
                "transform": {
                    "attributes": {
                        "value": {
                            "attributes": {
                                "values": [
                                    {
                                        "attributes": {
                                            "begin": 2,
                                            "end": 4,
                                            "input": {
                                                "attributes": {
                                                    "name": "sysdate"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 10,
                                            "end": 12,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 0,
                                            "end": 2,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 4,
                                            "end": 6,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 6,
                                            "end": 8,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 2,
                                            "end": 4,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 8,
                                            "end": 10,
                                            "input": {
                                                "attributes": {
                                                    "name": "key"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    },
                                    {
                                        "attributes": {
                                            "begin": 0,
                                            "end": 2,
                                            "input": {
                                                "attributes": {
                                                    "name": "sysdate"
                                                },
                                                "type": "identityAttribute"
                                            }
                                        },
                                        "type": "substring"
                                    }
                                ]
                            },
                            "type": "concat"
                        }
                    },
                    "type": "static"
                },
                "type": "secret"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "IIQDisabled",
                "transform": {
                    "attributes": {
                        "value": "false"
                    },
                    "type": "static"
                },
                "type": "boolean"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "primaryGroupDN",
                "transform": {
                    "attributes": {
                        "value": ""
                    },
                    "type": "static"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "description",
                "transform": {
                    "attributes": {
                        "name": "title"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "telephoneNumber",
                "transform": {
                    "attributes": {
                        "name": "phone"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msNPAllowDialin",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "homeMDB",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "mailNickname",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msExchHideFromAddressLists",
                "transform": null,
                "type": "boolean"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "SipAddress",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "SipDomain",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "SipAddressType",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msNPCallingStationID",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msRADIUSCallbackNumber",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "company",
                "transform": {
                    "attributes": {
                        "name": "company"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "mobile",
                "transform": {
                    "attributes": {
                        "name": "phone"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "department",
                "transform": {
                    "attributes": {
                        "name": "departmentname"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "postOfficeBox",
                "transform": {
                    "attributes": {
                        "name": "locationcode"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msRADIUSFramedRoute",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "msRADIUSFramedIPAddress",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "RegistrarPool",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "accountExpires",
                "transform": {
                    "attributes": {
                        "name": "adaccountexpires"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "employeeID",
                "transform": {
                    "attributes": {
                        "name": "ademployeeid"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "employeeType",
                "transform": {
                    "attributes": {
                        "name": "ademployeetype"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "title",
                "transform": {
                    "attributes": {
                        "name": "title"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            },
            {
                "attributes": {
                    "cloudMaxSize": "40",
                    "cloudMaxUniqueChecks": "50",
                    "cloudRequired": "true",
                    "template": "$(emailprefix)$(uniqueCounter)@SepQA.lab"
                },
                "isRequired": false,
                "name": "userPrincipalName",
                "transform": {
                    "attributes": {
                        "name": "Create Unique LDAP Attribute"
                    },
                    "type": "rule"
                },
                "type": "string"
            },
            {
                "attributes": {
                    "template": "$(emailprefix)$(uniqueCounter)@SepQA.lab"
                },
                "isRequired": false,
                "name": "mail",
                "transform": {
                    "attributes": {
                        "value": "$userPrincipalName"
                    },
                    "type": "static"
                },
                "type": "string"
            }
        ],
        "name": "Account",
        "usage": "Create",
        "validPolicy": true
    },
    {
        "description": null,
        "fields": [
            {
                "attributes": {},
                "isRequired": false,
                "name": "distinguishedName",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "sAMAccountName",
                "transform": null,
                "type": "string"
            }
        ],
        "name": "Create Group",
        "usage": "CreateGroup",
        "validPolicy": false
    },
    {
        "description": null,
        "fields": [
            {
                "attributes": {},
                "isRequired": false,
                "name": "GroupType",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "GroupScope",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "description",
                "transform": null,
                "type": "string"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "mailNickname",
                "transform": null,
                "type": "string"
            }
        ],
        "name": "Update Group",
        "usage": "UpdateGroup",
        "validPolicy": false
    },
    {
        "description": null,
        "fields": [
            {
                "attributes": {},
                "isRequired": false,
                "name": "memberOf",
                "transform": {
                    "attributes": {
                        "values": [
                            "CN=Domain Users,CN=Users,DC=SepQA,DC=lab"
                        ]
                    },
                    "type": "static"
                },
                "type": "String"
            },
            {
                "attributes": {},
                "isRequired": false,
                "name": "AC_NewParent",
                "transform": {
                    "attributes": {
                        "value": "OU=Disabled Users,OU=Special Accounts,OU=Contractors,OU=Sephora,DC=SepQA,DC=lab"
                    },
                    "type": "static"
                },
                "type": "string"
            }
        ],
        "name": "Account",
        "usage": "Disable",
        "validPolicy": true
    },
    {
        "description": null,
        "fields": [
            {
                "attributes": {},
                "isRequired": false,
                "name": "AC_NewParent",
                "transform": {
                    "attributes": {
                        "name": "organizationalunit"
                    },
                    "type": "identityAttribute"
                },
                "type": "string"
            }
        ],
        "name": "Account",
        "usage": "Enable",
        "validPolicy": true
    }
]